@numchapentry{Introduction}{1}{Introduction}{1}
@numsecentry{What is Bash?}{1.1}{What is Bash?}{1}
@numsecentry{What is a shell?}{1.2}{What is a shell?}{1}
@numchapentry{Definitions}{2}{Definitions}{3}
@numchapentry{Basic Shell Features}{3}{Basic Shell Features}{5}
@numsecentry{Shell Syntax}{3.1}{Shell Syntax}{5}
@numsubsecentry{Shell Operation}{3.1.1}{Shell Operation}{5}
@numsubsecentry{Quoting}{3.1.2}{Quoting}{6}
@numsubsubsecentry{Escape Character}{3.1.2.1}{Escape Character}{6}
@numsubsubsecentry{Single Quotes}{3.1.2.2}{Single Quotes}{6}
@numsubsubsecentry{Double Quotes}{3.1.2.3}{Double Quotes}{6}
@numsubsubsecentry{ANSI-C Quoting}{3.1.2.4}{ANSI-C Quoting}{6}
@numsubsubsecentry{Locale-Specific Translation}{3.1.2.5}{Locale Translation}{7}
@numsubsecentry{Comments}{3.1.3}{Comments}{9}
@numsecentry{Shell Commands}{3.2}{Shell Commands}{9}
@numsubsecentry{Reserved Words}{3.2.1}{Reserved Words}{9}
@numsubsecentry{Simple Commands}{3.2.2}{Simple Commands}{9}
@numsubsecentry{Pipelines}{3.2.3}{Pipelines}{10}
@numsubsecentry{Lists of Commands}{3.2.4}{Lists}{10}
@numsubsecentry{Compound Commands}{3.2.5}{Compound Commands}{11}
@numsubsubsecentry{Looping Constructs}{3.2.5.1}{Looping Constructs}{11}
@numsubsubsecentry{Conditional Constructs}{3.2.5.2}{Conditional Constructs}{12}
@numsubsubsecentry{Grouping Commands}{3.2.5.3}{Command Grouping}{17}
@numsubsecentry{Coprocesses}{3.2.6}{Coprocesses}{18}
@numsubsecentry{GNU Parallel}{3.2.7}{GNU Parallel}{19}
@numsecentry{Shell Functions}{3.3}{Shell Functions}{19}
@numsecentry{Shell Parameters}{3.4}{Shell Parameters}{21}
@numsubsecentry{Positional Parameters}{3.4.1}{Positional Parameters}{23}
@numsubsecentry{Special Parameters}{3.4.2}{Special Parameters}{23}
@numsecentry{Shell Expansions}{3.5}{Shell Expansions}{24}
@numsubsecentry{Brace Expansion}{3.5.1}{Brace Expansion}{24}
@numsubsecentry{Tilde Expansion}{3.5.2}{Tilde Expansion}{25}
@numsubsecentry{Shell Parameter Expansion}{3.5.3}{Shell Parameter Expansion}{26}
@numsubsecentry{Command Substitution}{3.5.4}{Command Substitution}{34}
@numsubsecentry{Arithmetic Expansion}{3.5.5}{Arithmetic Expansion}{35}
@numsubsecentry{Process Substitution}{3.5.6}{Process Substitution}{35}
@numsubsecentry{Word Splitting}{3.5.7}{Word Splitting}{36}
@numsubsecentry{Filename Expansion}{3.5.8}{Filename Expansion}{36}
@numsubsubsecentry{Pattern Matching}{3.5.8.1}{Pattern Matching}{37}
@numsubsecentry{Quote Removal}{3.5.9}{Quote Removal}{39}
@numsecentry{Redirections}{3.6}{Redirections}{39}
@numsubsecentry{Redirecting Input}{3.6.1}{}{40}
@numsubsecentry{Redirecting Output}{3.6.2}{}{40}
@numsubsecentry{Appending Redirected Output}{3.6.3}{}{40}
@numsubsecentry{Redirecting Standard Output and Standard Error}{3.6.4}{}{41}
@numsubsecentry{Appending Standard Output and Standard Error}{3.6.5}{}{41}
@numsubsecentry{Here Documents}{3.6.6}{}{41}
@numsubsecentry{Here Strings}{3.6.7}{}{42}
@numsubsecentry{Duplicating File Descriptors}{3.6.8}{}{42}
@numsubsecentry{Moving File Descriptors}{3.6.9}{}{42}
@numsubsecentry{Opening File Descriptors for Reading and Writing}{3.6.10}{}{42}
@numsecentry{Executing Commands}{3.7}{Executing Commands}{43}
@numsubsecentry{Simple Command Expansion}{3.7.1}{Simple Command Expansion}{43}
@numsubsecentry{Command Search and Execution}{3.7.2}{Command Search and Execution}{43}
@numsubsecentry{Command Execution Environment}{3.7.3}{Command Execution Environment}{44}
@numsubsecentry{Environment}{3.7.4}{Environment}{45}
@numsubsecentry{Exit Status}{3.7.5}{Exit Status}{45}
@numsubsecentry{Signals}{3.7.6}{Signals}{46}
@numsecentry{Shell Scripts}{3.8}{Shell Scripts}{47}
@numchapentry{Shell Builtin Commands}{4}{Shell Builtin Commands}{49}
@numsecentry{Bourne Shell Builtins}{4.1}{Bourne Shell Builtins}{49}
@numsecentry{Bash Builtin Commands}{4.2}{Bash Builtins}{57}
@numsecentry{Modifying Shell Behavior}{4.3}{Modifying Shell Behavior}{69}
@numsubsecentry{The Set Builtin}{4.3.1}{The Set Builtin}{69}
@numsubsecentry{The Shopt Builtin}{4.3.2}{The Shopt Builtin}{73}
@numsecentry{Special Builtins}{4.4}{Special Builtins}{80}
@numchapentry{Shell Variables}{5}{Shell Variables}{81}
@numsecentry{Bourne Shell Variables}{5.1}{Bourne Shell Variables}{81}
@numsecentry{Bash Variables}{5.2}{Bash Variables}{81}
@numchapentry{Bash Features}{6}{Bash Features}{94}
@numsecentry{Invoking Bash}{6.1}{Invoking Bash}{94}
@numsecentry{Bash Startup Files}{6.2}{Bash Startup Files}{96}
@numsecentry{Interactive Shells}{6.3}{Interactive Shells}{97}
@numsubsecentry{What is an Interactive Shell?}{6.3.1}{What is an Interactive Shell?}{98}
@numsubsecentry{Is this Shell Interactive?}{6.3.2}{Is this Shell Interactive?}{98}
@numsubsecentry{Interactive Shell Behavior}{6.3.3}{Interactive Shell Behavior}{98}
@numsecentry{Bash Conditional Expressions}{6.4}{Bash Conditional Expressions}{99}
@numsecentry{Shell Arithmetic}{6.5}{Shell Arithmetic}{101}
@numsecentry{Aliases}{6.6}{Aliases}{103}
@numsecentry{Arrays}{6.7}{Arrays}{103}
@numsecentry{The Directory Stack}{6.8}{The Directory Stack}{105}
@numsubsecentry{Directory Stack Builtins}{6.8.1}{Directory Stack Builtins}{106}
@numsecentry{Controlling the Prompt}{6.9}{Controlling the Prompt}{107}
@numsecentry{The Restricted Shell}{6.10}{The Restricted Shell}{109}
@numsecentry{Bash and POSIX}{6.11}{Bash POSIX Mode}{109}
@numsubsecentry{What is POSIX?}{6.11.1}{}{109}
@numsubsecentry{Bash POSIX Mode}{6.11.2}{}{110}
@numsecentry{Shell Compatibility Mode}{6.12}{Shell Compatibility Mode}{114}
@numchapentry{Job Control}{7}{Job Control}{118}
@numsecentry{Job Control Basics}{7.1}{Job Control Basics}{118}
@numsecentry{Job Control Builtins}{7.2}{Job Control Builtins}{119}
@numsecentry{Job Control Variables}{7.3}{Job Control Variables}{121}
@numchapentry{Command Line Editing}{8}{Command Line Editing}{122}
@numsecentry{Introduction to Line Editing}{8.1}{Introduction and Notation}{122}
@numsecentry{Readline Interaction}{8.2}{Readline Interaction}{122}
@numsubsecentry{Readline Bare Essentials}{8.2.1}{Readline Bare Essentials}{123}
@numsubsecentry{Readline Movement Commands}{8.2.2}{Readline Movement Commands}{123}
@numsubsecentry{Readline Killing Commands}{8.2.3}{Readline Killing Commands}{124}
@numsubsecentry{Readline Arguments}{8.2.4}{Readline Arguments}{124}
@numsubsecentry{Searching for Commands in the History}{8.2.5}{Searching}{124}
@numsecentry{Readline Init File}{8.3}{Readline Init File}{125}
@numsubsecentry{Readline Init File Syntax}{8.3.1}{Readline Init File Syntax}{125}
@numsubsecentry{Conditional Init Constructs}{8.3.2}{Conditional Init Constructs}{134}
@numsubsecentry{Sample Init File}{8.3.3}{Sample Init File}{136}
@numsecentry{Bindable Readline Commands}{8.4}{Bindable Readline Commands}{139}
@numsubsecentry{Commands For Moving}{8.4.1}{Commands For Moving}{139}
@numsubsecentry{Commands For Manipulating The History}{8.4.2}{Commands For History}{140}
@numsubsecentry{Commands For Changing Text}{8.4.3}{Commands For Text}{142}
@numsubsecentry{Killing And Yanking}{8.4.4}{Commands For Killing}{143}
@numsubsecentry{Specifying Numeric Arguments}{8.4.5}{Numeric Arguments}{144}
@numsubsecentry{Letting Readline Type For You}{8.4.6}{Commands For Completion}{145}
@numsubsecentry{Keyboard Macros}{8.4.7}{Keyboard Macros}{146}
@numsubsecentry{Some Miscellaneous Commands}{8.4.8}{Miscellaneous Commands}{147}
@numsecentry{Readline vi Mode}{8.5}{Readline vi Mode}{149}
@numsecentry{Programmable Completion}{8.6}{Programmable Completion}{150}
@numsecentry{Programmable Completion Builtins}{8.7}{Programmable Completion Builtins}{152}
@numsecentry{A Programmable Completion Example}{8.8}{A Programmable Completion Example}{156}
@numchapentry{Using History Interactively}{9}{Using History Interactively}{159}
@numsecentry{Bash History Facilities}{9.1}{Bash History Facilities}{159}
@numsecentry{Bash History Builtins}{9.2}{Bash History Builtins}{159}
@numsecentry{History Expansion}{9.3}{History Interaction}{161}
@numsubsecentry{Event Designators}{9.3.1}{Event Designators}{162}
@numsubsecentry{Word Designators}{9.3.2}{Word Designators}{163}
@numsubsecentry{Modifiers}{9.3.3}{Modifiers}{164}
@numchapentry{Installing Bash}{10}{Installing Bash}{165}
@numsecentry{Basic Installation}{10.1}{Basic Installation}{165}
@numsecentry{Compilers and Options}{10.2}{Compilers and Options}{166}
@numsecentry{Compiling For Multiple Architectures}{10.3}{Compiling For Multiple Architectures}{166}
@numsecentry{Installation Names}{10.4}{Installation Names}{167}
@numsecentry{Specifying the System Type}{10.5}{Specifying the System Type}{167}
@numsecentry{Sharing Defaults}{10.6}{Sharing Defaults}{167}
@numsecentry{Operation Controls}{10.7}{Operation Controls}{168}
@numsecentry{Optional Features}{10.8}{Optional Features}{168}
@appentry{Reporting Bugs}{A}{Reporting Bugs}{174}
@appentry{Major Differences From The Bourne Shell}{B}{Major Differences From The Bourne Shell}{175}
@appsecentry{Implementation Differences From The SVR4.2 Shell}{B.1}{}{180}
@appentry{GNU Free Documentation License}{C}{GNU Free Documentation License}{181}
@appentry{Indexes}{D}{Indexes}{189}
@appsecentry{Index of Shell Builtin Commands}{D.1}{Builtin Index}{189}
@appsecentry{Index of Shell Reserved Words}{D.2}{Reserved Word Index}{190}
@appsecentry{Parameter and Variable Index}{D.3}{Variable Index}{191}
@appsecentry{Function Index}{D.4}{Function Index}{193}
@appsecentry{Concept Index}{D.5}{Concept Index}{195}
